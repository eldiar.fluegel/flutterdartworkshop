class Note {
  NoteState state;
  String headline;
  String text;

  Note(this.state, this.headline, this.text);

}

enum NoteState {
    Green,
    Yellow,
    Red
}